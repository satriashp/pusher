package com.example.shp.pusher

/*
 * Created by SHP on 26/01/2018.
 */
data class Message(var name: String, var message: String)