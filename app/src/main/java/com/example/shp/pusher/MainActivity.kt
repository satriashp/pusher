package com.example.shp.pusher

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.gson.Gson
import com.pusher.android.PusherAndroid
import com.pusher.android.PusherAndroidOptions
import com.pusher.android.notifications.PushNotificationRegistration
import com.pusher.android.notifications.interests.InterestSubscriptionChangeListener
import com.pusher.android.notifications.tokens.PushNotificationRegistrationListener
import com.pusher.client.channel.*
import com.pusher.client.util.HttpAuthorizer
import java.lang.Exception


class MainActivity : AppCompatActivity() {
    private val tag: String = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val options = PusherAndroidOptions()
        options.setCluster("ap1")
        val pusher = PusherAndroid("b2db4a926e5768519444", options)

        val authorized = HttpAuthorizer("http://c3031260.ngrok.io/api/auth/pusher")
        val header: MutableMap<String, String> = mutableMapOf()
        header["Authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRkNDY5YTk0OWI2ZDQxYWFmOGVjMWZhZjNhOGI3MmI0ODNiNzQ1M2ZlNjg1ZmEyYTFkMGNhNTdkMjI0ZmEyMWUwNDAzMDYxOWU0Y2RlZDhlIn0.eyJhdWQiOiIxIiwianRpIjoiZGQ0NjlhOTQ5YjZkNDFhYWY4ZWMxZmFmM2E4YjcyYjQ4M2I3NDUzZmU2ODVmYTJhMWQwY2E1N2QyMjRmYTIxZTA0MDMwNjE5ZTRjZGVkOGUiLCJpYXQiOjE1MTY5NjQ0MTcsIm5iZiI6MTUxNjk2NDQxNywiZXhwIjoxNTQ4NTAwNDE3LCJzdWIiOiIzIiwic2NvcGVzIjpbImRvY3RvciIsImNvbnN1bHRhdGlvbiIsImNhbi1hZGQtbm90ZSIsImNhbi1nZXQtbGlzdC1kb2N0b3IiXX0.BmztRgV3Nv0Ufp6-1-86N_A55HwJZg5LjBCXTw4ElTXVlLz1W-Wodr6MJ01dyMvlDohkbO3_k7H4_TiKaq3Hjyx9yqo0kbEQaETAEYXf5V5P5se0MxHQ5BhRqz02ibGSLHP4eitsaVW3yGlffgh7F7IG0X-vfqizGNlzYhFCUBaKCAmd-XwrWTS8P1X6BI-GFPWCYoO4dCKfN5Pb_9Sbg99NAes2KsCzVK_HSGT0Uo9ho3yioH-y0tJEhcgXE94h7nnr94S3JOjmu8kqj0LeM6KGtg8WqxUjPtvVRoA4yH2wn-4N6_YyFTLBdvAQh0P33v2WjPXJuItNVn9JACl_lmv8yxa7kbqZaPYQYabMRzeDI03pBLL6yIFHx6I6CTasPbWvhE4BW4QRL7BVtgZV6t8kxH6NCjK2BXIz4mubjgdANQg-W8n70eRMreVaubH9O0BcMHiQmkhzte3nAukHTLRWOSjbUNKcugoTaaMj40nEMyFX188iTImSzZqni7d7ikcAxA0HIwbctr0TJUiBEJmhHbBPOR6CAsgZ3k0NH7qAe61vJjaM-IdpUIdm09oIvqJ5MFIe2JEFaq6e6LY6cQcGIgkjr4USv_JwDMe0l3T0nhUMC3BF20jssrA2W3CGLpZDIwh4CfJIq-G6zz1nPNYNSNM_rsOTG-HVbLWMkhU"
        authorized.setHeaders(header)
        options.authorizer = authorized

        val channel = pusher.subscribe("foobar")
        channel.bind("foo") { channelName, eventName, data ->
            val gson = Gson()
            val message = gson.fromJson(data, Message::class.java)
            println(message)
        }

        val privateChannel = pusher.subscribePrivate("private-foobar")
        privateChannel.bind("foo", object: PrivateChannelEventListener{
            override fun onEvent(p0: String?, p1: String?, p2: String?) {
                runOnUiThread({
                    val gson = Gson()
                    val message = gson.fromJson(p2, Message::class.java)
                    println(message)
                })
            }

            override fun onAuthenticationFailure(p0: String?, p1: Exception?) {
                Log.e("onAuthenticationFailure", p0)
                Log.e("onAuthenticationFailure", p1?.message)
            }

            override fun onSubscriptionSucceeded(p0: String?) {
                Log.e("onSubscriptionSucceeded", p0)
            }
        })

        val presenceChannel = pusher.subscribePresence("presence-foobar")
        presenceChannel.bind("foo", object: PresenceChannelEventListener{
            override fun onUsersInformationReceived(p0: String?, p1: MutableSet<User>?) {
                Log.e("userInfo", p0)
            }

            override fun userUnsubscribed(p0: String?, p1: User?) {
                Log.e("userUnsubscribed", p0)
            }

            override fun userSubscribed(p0: String?, p1: User?) {
                Log.e("userSubscribed", p0)
            }

            override fun onEvent(p0: String?, p1: String?, p2: String?) {
                runOnUiThread({
                    val gson = Gson()
                    val message = gson.fromJson(p2, Message::class.java)
                    println(message)
                })
            }

            override fun onAuthenticationFailure(p0: String?, p1: Exception?) {
                Log.e("onAuthenticationFailure", p0)
                Log.e("onAuthenticationFailure", p1?.message)
            }

            override fun onSubscriptionSucceeded(p0: String?) {
                Log.e("onSubscriptionSucceeded", p0)
            }
        })

        pusher.connect()

        val nativePusher: PushNotificationRegistration = pusher.nativePusher()
        nativePusher.registerFCM(this, object : PushNotificationRegistrationListener {
            override fun onFailedRegistration(statusCode: Int, response: String?) {
                Log.i(tag, response)
            }

            override fun onSuccessfulRegistration() {
                Log.i(tag, "YEEEEEEEEEEEEEEY")
            }
        })

        nativePusher.subscribe("DO2018012600622897", object : InterestSubscriptionChangeListener {
            override fun onSubscriptionChangeSucceeded() {
                Log.i(tag, "Success! I love kittens!")
            }

            override fun onSubscriptionChangeFailed(statusCode: Int, response: String) {
                println(":(: received $statusCode with$response")
            }
        })

        nativePusher.setFCMListener {
            Log.i(tag, "Message received")
        }
    }
}
